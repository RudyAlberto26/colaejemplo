import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class TDACola {

	private JFrame frame;
	private JTextField txt1;
	JTextArea area = new JTextArea();
	Colas obj = new Colas();
	int valor;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TDACola window = new TDACola();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TDACola() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		txt1 = new JTextField();
		txt1.setBounds(10, 11, 151, 20);
		frame.getContentPane().add(txt1);
		txt1.setColumns(10);
		
		JButton btn1 = new JButton("Agregar");
		btn1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				valor = Integer.parseInt(txt1.getText().toString());
				obj.llenarCola(valor);
			}
		});
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btn1.setBounds(180, 10, 89, 23);
		frame.getContentPane().add(btn1);
		
		JButton btn2 = new JButton("Imprimir");
		btn2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				obj.regresarCola();
				area.setText(obj.getCadena()+"");
			}
		});
		btn2.setBounds(278, 10, 71, 23);
		frame.getContentPane().add(btn2);
		
		area = new JTextArea();
		area.setBounds(34, 76, 339, 158);
		frame.getContentPane().add(area);
	}
}
